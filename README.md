# AEROTHEMEPLASMA FOR KDE 6

## WARNING: This version is very early WIP and is not fully finished. Proceed with caution.

## Microsoft® Windows™ is a registered trademark of Microsoft® Corporation. This name is used for referential use only, and does not aim to usurp copyrights from Microsoft. Microsoft Ⓒ 2024 All rights reserved. All resources belong to Microsoft Corporation.

## Introduction

This is a project which aims to recreate the look and feel of Windows 7 as much as possible on KDE Plasma, whilst adapting the design to fit in with modern features provided by Plasma and Linux.
It is still in heavy development and testing. This is the port to KDE Plasma 6, and it has currently only been tested on:

1. Arch Linux x64
2. Plasma 6.1.3, KDE Frameworks 6.4.0, Qt 6.7.2
3. 96 DPI Scaling, single monitor

**This release is meant for early adopters, debuggers and developers alike. This port lacks certain components which still need to be ported over to KDE Plasma 6. I am not responsible for broken systems, please proceed with caution.**

See [INSTALL.md](./INSTALL.md) for a really quick, half baked and incomprehensive installation guide. The rest of the project and the appropriate complete documentation will be available in due time.

The Plasma 5 version of ATP is available as a tag in this repository, however it is unmaintained and no longer supported.

<a href='https://ko-fi.com/M4M2NJ9PJ' target='_blank'><img height='36' style='border:0px;height:36px;' src='https://storage.ko-fi.com/cdn/kofi2.png?v=3' border='0' alt='Buy Me a Coffee at ko-fi.com' /></a>
